BASENAME	=slides
FIGURESDIR	= res/figures
FIGURESSRC	:= $(wildcard $(FIGURESDIR)/*.tex)
FIGURES		:= $(FIGURESSRC:$(FIGURESDIR)/%.tex=$(FIGURESDIR)/%.pdf)

.PHONY: default clean compile all
all: figures compile

figures: $(FIGURES)

compile: $(BASENAME).pdf

tex: $(BASENAME).tex

$(FIGURES): $(FIGURESDIR)/%.pdf : $(FIGURESDIR)/%.tex includes.tex
	latexmk -pdf $< -outdir=$(FIGURESDIR)

$(BASENAME).pdf: $(BASENAME).tex $(FIGURES)
	latexmk -pdf -shell-escape $<

$(BASENAME).tex: $(BASENAME).md $(FIGURES)
	pandoc -s -t latex -t beamer $< -o $@

view: $(BASENAME).pdf
	evince $^

clean:
	latexmk -C; cd $(FIGURESDIR); latexmk -C
	rm -f $(BASENAME).pdf $(BASENAME).tex
