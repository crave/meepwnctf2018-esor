---
title: Solving a fixed-key challenge without the key
subtitle: or -- how to padding oracle
aspectratio: 169
date: \today
author: malet $\land$ crave
theme: metropolis
urlcolor: blue
header-includes: |
    \DeclareMathOperator{\sha}{SHA256}
    \DeclareMathOperator{\bool}{Bool}
    \DeclareMathOperator{\aes}{AES}
    \DeclareMathOperator{\cbc}{CBC}
    \DeclareMathOperator{\mac}{MAC}
    \DeclareMathOperator{\prefix}{prefix}
    \DeclareMathOperator{\concat}{||}

    \usepackage{tikz}
    \newcounter{mybox}
    \newcommand\ColorBox[2][]{%
    \stepcounter{mybox}%
    \node[draw=red!70!black,fill=red!20,align=left,#1] (box\themybox) {#2};
    }
    \usepackage{multirow}

    \usepackage{minted}
    \usemintedstyle{borland}

    \usepackage{tikz}
    \usetikzlibrary{calc,tikzmark,decorations.pathreplacing,shapes,positioning,fit}

    \input{includes.tex}
    \DeclareUnicodeCharacter{2718}{\ballotx}
    \newcommand\Checkmark{\textcolor{ternary}{\textbf{\checkmark}}}

    \pgfdeclarelayer{bg}
    \pgfsetlayers{bg,main}
---

# Given Description

```
Howdy, howdy...
nc 206.189.92.209 12345
[Attachments]
```

# The Challenge (Encryption)

```
nc 206.189.92.209 12345

Welcome to our super secure enc/dec server. 
We use hmac, so, plz don't hack us (and you can't). Thanks.
Choose one:
1. encrypt data
2. decrypt data
3. quit
1
prefix: hello
suffix: world
41d032627e632e3a9daad7e6b3001e593e5c573cc [ ...MORE BYTES ]
```

# The Challenge (Decryption)

```
Choose one:
1. encrypt data
2. decrypt data
3. quit
2
data: 41d032627e632e3a9daad7e6b3001e593e5c573cc [ ...MORE BYTES]
OK
```

# A First Look {.fragile}

\begin{minted}{python}
encrypt_key = '\xff' * 32
secret = 'MeePwnCTF{#flag_here#}'
hmac_secret = ''
blocksize = 16
hmac_size = 20
\end{minted}

"\textit{Don't look at the fixed key, it is not a fixed key challenge}" said someone. \
And we listened to them...

# What we know by now
<!-- TODO: move the items to the top -->

* The start of the flag
* The block and key sizes

# An Abstract View

\begin{align*}
    &\tikzmark{encbeg}\text{msg} = \tikzmark{pbeg}\text{prefix}\tikzmark{pend} \concat \text{secret} \concat \tikzmark{sbeg}\text{suffix}\tikzmark{send} \\
    &\text{mac} = \mac(\text{msg}) \\[1em]
    &\tikzmark{encend}\text{cipher} = \aes_{\cbc}(K, IV, \text{msg} \concat \text{mac}) \\[1.5em]
    &\tikzmark{decbeg}\visible<2->{\text{plain} = \aes^{-1}_{\cbc}(K, IV, \text{cipher}) = \text{msg} \concat \text{mac}} \\
    &\tikzmark{decend}\visible<2->{\text{output} = \mac(\text{msg}) \stackrel{?}{=} \text{mac}}
\end{align*}

\begin{tikzpicture}[remember picture, overlay]
    \coordinate (off) at (0,+0.8em);
    \coordinate (pbeg) at ( $ (pic cs:pbeg) + (off) $ );
    \coordinate (pend) at ( $ (pic cs:pend) + (off) $ );
    \coordinate (sbeg) at ( $ (pic cs:sbeg) + (off) $ );
    \coordinate (send) at ( $ (pic cs:send) + (off) $ );
    \draw (pbeg) -- (pend);
    \draw (sbeg) -- (send);
    \coordinate (phalf) at ( $ (pbeg)!0.5!(pend) $ );
    \coordinate (shalf) at ( $ (sbeg)!0.5!(send) $ );
    \coordinate (middle) at ( $ (phalf)!0.5!(shalf) + (0,+20pt) $ );
    \draw[-latex] (middle) to[out=270,in=90] (phalf);
    \draw[-latex] (middle) node[above] {user controlled} to[out=270,in=90] (shalf) ;

    \draw[decorate,decoration={brace,mirror}] ( $ (pic cs:encbeg) + (-0.2em,0.6em) $ ) -- node[midway,left]{Encryption} ( $ (pic cs:encend) + (-0.2em,0) $ );
    \visible<2->{\draw[decorate,decoration={brace,mirror}] ( $ (pic cs:decbeg) + (-0.2em,0.6em) $ ) -- node[midway,left]{Decryption} ( $ (pic cs:decend) + (-0.2em,0) $ );}
\end{tikzpicture}

# What we know by now
<!-- TODO: move the items to the top -->

* \textcolor{faded}{The start of the flag}
* \textcolor{faded}{The block and key sizes}
* CBC Mode
* Weird MAC construction
* Mac-then-Encrypt

# Cipher Block Chaining -- encryption

![CBC encryption](./res/figures/CBC_encryption.pdf)

# Cipher Block Chaining -- decryption

![CBC decryption](./res/figures/CBC_decryption.pdf)

# Padding

\begin{figure}[h]
    \hfill\includegraphics[height=.19\textheight]{./res/figures/padding.pdf}
    \label{fig:padding}
\end{figure}

. . .

\begin{figure}[h]
    \hfill\includegraphics[height=.19\textheight]{./res/figures/padding-broken.pdf}
    \label{fig:padding}
\end{figure}

. . .

\begin{figure}[h]
    \hfill\includegraphics[height=.19\textheight]{./res/figures/padding-horrible.pdf}
    \label{fig:padding}
\end{figure}

# A Second Look {.fragile}

\begin{minted}{python}
data = _aes.decrypt(data[blocksize:])
data = unpad(data)
plaintext = data[:-hmac_size]
mac = data[-hmac_size:]
computed = compute_hmac(plaintext)
if mac == computed:
    return True
else:
    return False
\end{minted}

This looks like an oracle

# A Padding Oracle

\begin{figure}[h]
    \centering
    \includegraphics{./res/figures/padding-oracle.pdf}
    \label{fig:padding}
\end{figure}

# What we know by now
<!-- TODO: move the items to the top -->

* \textcolor{faded}{The start of the flag}
* \textcolor{faded}{The block and key sizes}
* \textcolor{faded}{CBC Mode}
* \textcolor{faded}{Weird MAC construction}
* \textcolor{faded}{Mac-then-Encrypt}
* Padding Oracle

# Attack preliminaries

\begin{enumerate}
    \item Compute the empty $\mac$ blocks (there is no key) \visible<2->{\Checkmark}
\item Get the encrypted padded $\mac$ blocks \visible<3->{\Checkmark}
\vspace{1em}
\item Get the Schrottblock \visible<4->{\Checkmark}
\vspace{1em}
\item Get the target block \visible<5->{\Checkmark}
\end{enumerate}

# Attack explained

\begin{figure}
\centering
\begin{tikzpicture}
    \tikzstyle{block} = [rectangle,draw,minimum width=8em, minimum height=2em, node distance=8em];
    \tikzstyle{byte} = [rectangle,draw,minimum width=1.5em, minimum height=2em];

    \visible<1->{
    \begin{scope}

    \node[block] (b0) at (0,0) {};
    \foreach \i in {1,...,3} {
        \edef\p{\number\numexpr\i-1\relax}
        \node[block, right of=b\p] (b\i) {};
    };

    \node[byte,xshift=-0.75em,fill=primary] (b2last) at (b2.east) {$n$};

    \visible<1>{
    \node[above of=b2last, yshift=1.5em] (know) {Know this byte};
    \draw[-latex] (know) -- (b2last);
    }

    \node[byte, minimum width=2em,xshift=1em] (macend) at (b1.west) {};
    \draw [decorate,decoration={brace,amplitude=10pt}]
    (b0.north west) --  node [above=1em] {MAC} (macend.north east);


    \draw [decorate,decoration={brace,amplitude=10pt}]
    (b2.north west) --  node [above=1em] {Schrott} (b2last.north west);
    \end{scope}
    }

    \visible<2->{
    \begin{scope}
    \node (f) [rectangle,draw,below of=b3] {$f^{-1}$};
    \draw[-latex] (b3.south) -- (f.north);
    \node (xor) [circle,draw,below of=f] {};
    \draw (xor.north) -- (xor.south);
    \draw (xor.east) -- (xor.west);
    \node[block] (pb0) at (0,-8em) {};
    \foreach \i in {1,...,3} {
        \edef\p{\number\numexpr\i-1\relax}
        \node[block, right of=pb\p] (pb\i) {};
    };
    \draw[-latex] (xor) -- (pb3);
    \draw[-latex] (f) -- (xor);
    \draw[-latex] ($(b2.south) - (0,1em)$) to[out=0,in=180] (xor);
    \draw[-latex] (b2.south) -- ($(b2.south) - (0,2em)$);

    \node[byte,xshift=-0.75em,fill=primary] (pb3last) at (pb3.east) {$b_1$};
    \visible<2>{
    \node[below of=pb3last, yshift=-1.5em] (notknow) {Guess this byte};
    \draw[-latex] (notknow) -- (pb3last);
    }

    \node[byte, minimum width=2em,xshift=1em] (pmacend) at (pb1.west) {};

    % Padding guesses
    \visible<3>{
        \coordinate (helper) at ($(pb1.south west) + (1,0)$) {};
    \draw [decorate,decoration={brace,amplitude=10pt}]
        (pb3last.south west) --  node [below=1em] {Padding} ($(pb1.south west) + (1,0)$);
        \begin{pgfonlayer}{bg}
            \visible<3>{
            \node[rectangle,fit=(pb0)(helper),inner sep=0,fill=lose] (lose) {};
            }
        \end{pgfonlayer}
    }
    \visible<4>{
        \coordinate (helper) at ($(pb1.south west) + (4,0)$) {};
    \draw [decorate,decoration={brace,amplitude=10pt}]
        (pb3last.south west) --  node [below=1em] {Padding} ($(pb1.south west) + (4,0)$);
        \begin{pgfonlayer}{bg}
            \visible<4>{
            \node[rectangle,fit=(pb0)(helper),inner sep=0,fill=lose] (lose) {};
            }
        \end{pgfonlayer}
    }
    \visible<5>{
        \coordinate (helper) at ($(pb1.south west) + (2.5,0)$) {};
    \draw [decorate,decoration={brace,amplitude=10pt}]
        (pb3last.south west) --  node [below=1em] {Padding} (helper);
        \begin{pgfonlayer}{bg}
            \visible<5>{
            \node[rectangle,fit=(pb0)(helper),inner sep=0,fill=lose] (lose) {};
            }
        \end{pgfonlayer}
    }
    \visible<6>{
    \draw [decorate,decoration={brace,amplitude=10pt}]
        (pb3last.south west) --  node [below=1em] {Padding} (pmacend.south east);
        \begin{pgfonlayer}{bg}
            \visible<6>{
            \node[rectangle,fit=(pb0)(pmacend),inner sep=0,fill=win] (win) {};
        }
        \end{pgfonlayer}
    }

    \end{scope}
    }
\end{tikzpicture}
\end{figure}
